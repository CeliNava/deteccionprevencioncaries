from django.contrib import admin
from .models import Paciente, Paciente_infante, Paciente_Adolecente_Adulto

admin.site.register(Paciente)
admin.site.register(Paciente_infante)
admin.site.register(Paciente_Adolecente_Adulto)


