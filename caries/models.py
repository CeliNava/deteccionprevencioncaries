from django.db import models

class Paciente(models.Model):
	nombre = models.CharField(max_length=50)
	apellidoPaterno = models.CharField(max_length=50)
	apellidoMaterno = models.CharField(max_length=50)
	SEXOS = (('F', 'Femenino'), ('M','Masculino'))
	sexo = models.CharField(choices=SEXOS, max_length=1)
	edad = models.PositiveSmallIntegerField()
	ZONAS = (('R', 'Rural'), ('U', 'Urbano'))
	zona = models.CharField(choices=ZONAS, max_length=1)
	personas_hogar = models.PositiveSmallIntegerField()
	escolaridad = [
		('Prim', 'Primaria'),
		('Secun', 'Secundaria'),
		('Prepa', 'Preparatoria'),
		('Lic', 'Licenciatura'),
		]
	ingreso_mensual = models.PositiveSmallIntegerField()
	fuente_consumo_agua = [
		('Publica', 'Distribución Pública'),
		('Manantial', 'Almacenamiento de manantial'),
		('Grifo', 'No bebo agua de grifo'),
		]
	FCEPILLADO = (('una', '0-1 veces por dia'), ('dos', '2-3 veces por dia'))
	cepillado = models.CharField(choices=FCEPILLADO, max_length=50)

	def __str__(self):
		return self.nombre


class Paciente_infante(models.Model):
	BEBIDAS = (('J', 'Jugo'), ('Sabor', 'Agua de Sabor'), ('S', 'Soda'), ('Leche', 'Leche de Sabor'), ('Natural', 'Agua Natural'))
	tipo_bebida = models.CharField(choices=BEBIDAS, max_length=50)
	FDULCES = (('UnaDos', '1-2 veces por semana'), ('TresCuatro', '3-4 veces por semana'), ('Cinco','Cinco o más veces por semana'))
	frecuencia_consumo_dulces = models.CharField(choices=FDULCES, max_length=50)

class Paciente_Adolecente_Adulto(models.Model):
	SUSTANCIAS_NOCIVAS = (('S', 'Si'), ('N', 'No'))
	sustancias_nocivas = models.CharField(choices=SUSTANCIAS_NOCIVAS, max_length=50)
